#include <Appkit/Appkit.h>
#include "Train.h"
#include "AppDelegate.h"

int main(int argc, char **argv) {
    int i = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    AppDelegate *delegate = [AppDelegate new];

    Train *trainToSouthNorwalk = [[Train alloc] initWithTrainNumber:1456];
    [trainToSouthNorwalk addStationStop:@"Harlem-125th Street"];
    [trainToSouthNorwalk addStationStop:@"Stamford"];
    [trainToSouthNorwalk addStationStop:@"South Norwalk"];
    [trainToSouthNorwalk addStationStop:@"East Norwalk"];
    [trainToSouthNorwalk addStationStop:@"Westport"];
    [trainToSouthNorwalk addStationStop:@"Green's Farms"];
    [trainToSouthNorwalk addStationStop:@"Southport"];
    [trainToSouthNorwalk addStationStop:@"Fairfield"];
    [trainToSouthNorwalk addStationStop:@"Fairfield Metro"];
    [trainToSouthNorwalk addStationStop:@"Bridgeport"];

    printf("This is train %d,\n\tthe express train to %s\n\tmaking the following station stops:\n",
        [[trainToSouthNorwalk number] intValue],
        [[trainToSouthNorwalk lastStop] UTF8String]);
    
    for (i = 0; i < [[trainToSouthNorwalk stations] count] - 1; i++) {
        printf("\t\t%s,\n", [[[trainToSouthNorwalk stations] objectAtIndex:i] UTF8String]);
    }

    printf("\t\tand %s\n\n", [[trainToSouthNorwalk lastStop] UTF8String]);

    [NSApplication sharedApplication];
    [NSApp setDelegate:delegate];
    [NSApp run];

    [delegate release];
    [pool drain];

    return 0;
}
