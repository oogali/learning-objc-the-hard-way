#include "AppDelegate.h"

@implementation AppDelegate

- (void)applicationWillFinishLaunching:(NSNotification *)notification {
    NSRect windowRect = NSMakeRect(10, 100, 400, 300); 
    int styleMask = NSTitledWindowMask | NSClosableWindowMask | NSResizableWindowMask;

    self.window = [[NSWindow alloc] initWithContentRect:windowRect styleMask:styleMask backing:NSBackingStoreBuffered defer:NO];
    [self.window setTitle:@"Testing New Window"];
    [self.window center];
    [self.window setCollectionBehavior:NSWindowCollectionBehaviorCanJoinAllSpaces];

    NSMenu *menuBar = [[[NSMenu alloc] init] autorelease];
    NSMenuItem *appMenuItem = [[[NSMenuItem alloc] init] autorelease];
    [menuBar addItem:appMenuItem];
    [NSApp setMainMenu:menuBar];

    NSMenu *appMenu = [[[NSMenu alloc] init] autorelease];
    NSMenuItem *quitMenuItem = [[[NSMenuItem alloc] initWithTitle:@"Quit" action:@selector(terminate:) keyEquivalent:@"q"] autorelease];
    [appMenu addItem:quitMenuItem];
    [appMenuItem setSubmenu:appMenu];

    NSRect screenRect = [[self.window screen] frame];
    NSLog(@"Screen is %f x %f", screenRect.size.width, screenRect.size.height);

    NSView *view = [[NSView alloc] initWithFrame:windowRect];
    [self.window.contentView addSubview:view];

    NSButton *easyButton = [[NSButton alloc] initWithFrame:NSMakeRect(10, 10, 150, 30)];
    [easyButton setTitle:@"That was easy..."];
    easyButton.bezelStyle = NSRoundedBezelStyle;
    [self.window.contentView addSubview:easyButton];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];

    [self.window makeKeyAndOrderFront:NSApp];
    [self.window makeMainWindow];

    [NSApp activateIgnoringOtherApps:YES];
    NSLog(@"%@", [[NSApplication sharedApplication] mainWindow]);
}

- (void)applicationWillTerminate:(NSNotification *)notification {
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)application {
    return YES;
}

@end
