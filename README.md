# Learning Objective-C the Hard Way

### Introduction

I've been trying to pick up Objective-C programming for some time now, but keep running into a sort of mental road block.

Some days, Xcode chews up too much RAM, and everything takes forever (and the iPhone Simulator creates a million zombie processes).

Other days, Google searches are fruitless, leading me to outdated blog posts, or sites that say "yeah dude, just cut and paste this".

So, this is my futile attempt at learning Objective-C on my own, no matter how many times I smash my head into the wall.
