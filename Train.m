#import "Train.h"

/*
 * This is the implementation of our previously defined interface,
 * located in the header file above.
 */
@implementation Train

/* Method Declaration for isStopped()
 * ----------------------------------
 * If this were raw C, the equivalent function declaration would look
 * something like this:
 *   bool isStopped(void *self) { ... }
 *
 * The class object is always passed as the first argument, and always
 * called self. Similar to passing a context variable as the first
 * argument to a C function (good ol' "ctx"), or how Python does
 * classes.
 */
- (BOOL)isStopped {
    return false;
}

/* Method Declaration, but with Arguments
 * --------------------------------------
 * The method declaration goes like so:
 *   (return type)methodName:(type)argument1, (type)argumentN, ...
 */
- (void)addStationStop:(NSString *)stationName {
    /*
     * We use a mutable array because, it's uh, mutable. The
     * regularly-named NSArray cannot be modified.
     *
     * Below, we allocate space for the array if it hasn't already
     * been done.
     *
     * Perhaps we should move this to the constructor?
     */
    if (!_stations) {
        self.stations = [[NSMutableArray alloc] init];
    }

    /*
     * There is no method that I can see that allows you to add an
     * object to an array per se, so you 'append' to an array, by
     * calling the arrayByAddingObject, which does what it says:
     *
     * creates a new array, containing your new object, and returns
     * that object.
     *
     * The way this function is named, it makes me itch for a profiler.
     */
    [self.stations addObject:stationName];
}

- (NSString *)nextStop {
    /*
     * If we don't have any stations defined, return nothing.
     */
    if (!_stations || [_stations count] <= 0) {
        return nil;
    }

    /* Return the first station in our array. */
    return [self.stations objectAtIndex:0];
}

- (NSString *)lastStop {
    /*
     * If we don't have a station array allocated, return nil.
     */
    if (!_stations) {
        return nil;
    }

    /* Return the last station in our array. */
    return [self.stations lastObject];
}

/* Class constructor (additional)
 * ------------------------------
 * In Objective-C, you don't override class constructors, you create
 * additional methods and call those instead.
 *
 * The default constructor in NSObject is called "init", which is why
 * you see a lot of
 *
 *   SomeObject* object = [SomeObject new];
 *
 * What that actually does, is something along the lines of:
 *
 *   SomeObject* object = [[SomeObject alloc] init];
 *
 * But coming back to the main point, if you want to initialize the
 * object using a non-standard constructor, you're going to declare
 * (below) and call your new constructor.
 *
 *   SomeObject* object = [[SomeObject alloc] initWithFunkyBass:bass];
 *
 * And in turn, your new constructor should call "init" of the
 * superclass, do its magic, and then return the self object.
 *
 * See below for a demonstration.
 */
- (id)initWithTrainNumber:(int)trainNumber {
    self = [super init];

	if (self) {
	    self.number = [NSNumber numberWithInt:trainNumber];
	}

	return self;
}

/* The end of our Train implementation */
@end
