/*
 * We need the Foundation framework, the base set of Objective-C
 * classes, so we import its header, which imports its headers.
 *
 * When you call #import, the compiler doesn't search /usr/include, but
 * /System/Library/Frameworks/$(name).framework/Headers/$(file)
 *
 * In this instance, the file we're importing (including), comes from
 * /System/Library/Frameworks/Foundation.framework/Headers/Foundation.h
 *
 * For a list of references that currently seems useful to intermediate
 * Obj-C programmers, see:
 * https://developer.apple.com/library/mac/#documentation/cocoa/reference/foundation/ObjC_classic/_index.html
 */
#import <Foundation/Foundation.h>

/*
 * So, this is how classes are defined, using the interface keyword,
 * while subclassing from the root object class, NSObject.
 */
@interface Train : NSObject;

/*
 * Property Declarations
 * ---------------------
 * Property declarations automatically generate getter/setter methods
 * (with no need anymore for @synthesize), and also add the property to
 * an internal data structure (that you can later enumerate by
 * retrieving said array).
 *
 * When declaring a property, you can set said property to be of
 * multiple types, depending on how you want this property to be (or
 * not be) accessed later:
 *
 *   readwrite: (DEFAULT) a getter and a setter are generated
 *
 *   readonly: a getter is generated, but not a setter
 *
 *   atomic: (DEFAULT) the getter will always return a valid value,
 *        even if another thread is currently in the middle of
 *        executing the setter method (and modifying the value)
 *
 *   nonatomic: no guarantees that it can return a valid value, but
 *        apparently a lot faster (due to lack of guarantee), appears
 *        to be default in most copy-pasta links
 *
 *   weak (or assign): the setter will simply assign the value to the
 *        property, so setSpeed:100 is equal to Train->speed = 100
 *
 *   strong (or retain): the setter will decrement the reference count
 *        on the old object, set the property to a pointer of the object
 *        you are passing in, and increment the passed object's
 *        reference count
 *
 *   copy: the setter will copy the passed object (creating a new
 *        object), and set the property's value to a pointer to the
 *        newly created object
 *
 * Objective-C has its own set of classes to replace the familiar
 * C primitives, because: objects.
 *
 * Also, everything is prefixed with "NS", as the birth of this 
 * code came from the NeXTStep days.
 */
@property (nonatomic, retain) NSNumber *number;
@property (nonatomic, assign) NSInteger speed;
@property (nonatomic, retain) NSMutableArray *stations;
@property (nonatomic, assign) BOOL inStation;

/*
 * Method Declarations
 * -------------------
 * This is a bit of a misnomer.
 *
 * You don't call functions, or invoke methods in Objective-C; you send
 * messages to an object. To which the object responds (or doesn't).
 *
 * Executing this:
 *   [anObject itsMethod];
 *
 * roughly translates into this in C:
 *   objc_msgSend(anObject, aHelperFunctionToFindMethod(itsMethod));
 *
 * Then, objc_msgSend traverses anObject's implementation, looking for
 * a declared message receiver named itsMethod, and executes it (rather,
 * passes control to it?)
 *
 * But I guess for ease sake, people still tend to refer to them as
 * functions or methods.
 */
- (BOOL)isStopped;
- (id)initWithTrainNumber:(int)trainNumber;
- (void)addStationStop:(NSString *)stationName;
- (NSString *)nextStop;
- (NSString *)lastStop;

/* The end of our Train interface declaration */
@end
