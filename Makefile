CFLAGS+=-ObjC -Wall -O3 -I.
LD=$(CC)
LIBS=-framework Foundation -framework Appkit

SRCS=$(wildcard *.m)
OBJS=$(SRCS:.m=.o)
BIN=train

all: $(BIN)

train: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	$(RM) $(BIN) $(OBJS)

PHONY: all
